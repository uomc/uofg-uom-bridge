import { Client, Events, GatewayIntentBits, Webhook } from "discord.js";
import "dotenv/config";

const { TOKEN, UOFG_CHANNEL, UOM_CHANNEL } = process.env;

const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent,
  ],
});

/** @type {import("discord.js").Channel | undefined} */
let uofgChannel = undefined;
/** @type {import("discord.js").Channel | undefined} */
let uomChannel = undefined;

client.login(TOKEN);

client.once(Events.ClientReady, () => {
  uofgChannel = client.channels.cache.get(UOFG_CHANNEL);
  uomChannel = client.channels.cache.get(UOM_CHANNEL);
  console.log(`Logged in as ${client.user.tag}`);
});

client.on(Events.MessageCreate, async (message) => {
  if (
    message.webhookId === null &&
    uofgChannel !== undefined &&
    uomChannel !== undefined
  ) {
    let channel = undefined;

    if (message.channel === uofgChannel) {
      channel = uomChannel;
    } else if (message.channel === uomChannel) {
      channel = uofgChannel;
    }

    channel
      ?.createWebhook({
        name: message.member.nickname ?? message.member.displayName,
        avatar: message.member.user.avatarURL(),
      })
      .then(
        /** @param {Webhook} webhook */
        async (webhook) => {
          await webhook.send(message.content);
          await webhook.delete();
        }
      )
      .catch(console.error);
  }
});
